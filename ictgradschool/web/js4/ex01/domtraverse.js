"use strict";

/* Your answer here */

var firstDiv = document.getElementsByClassName("text1");

firstDiv[0].style.backgroundColor = "pink";

var lastPara = document.getElementById("footer");

lastPara.style.color = "red";

var secondDiv = document.getElementsByClassName("text2");

secondDiv[0].style.visibility = "hidden";

var firstPara = document.getElementsByClassName("subtitle");

firstPara[0].innerHTML = "HELLO WORLD";

var button = document.getElementById("makeBold");
var allParaBold = document.querySelectorAll("p");

function doSomething(){

    for (var i=0; i < allParaBold.length; i ++) {
        allParaBold[i].style.fontWeight = "bold";
    }
}

button.onclick = doSomething;


